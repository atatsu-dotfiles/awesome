local awful = require("awful")

local prefs = require("prefs")

local capi = {
  ---@diagnostic disable-next-line: undefined-global
  client = client,
}

local modkey = prefs.modkey

local is_setup = false

local M
M = {
  init = function()
    if is_setup then return end
    is_setup = true
    awful.mouse.append_global_mousebindings(M.mouse)
  end
}

M.client = {
  awful.button(
    {}, 1,
    function(c)
      c:activate { context = "mouse_click" }
    end
  ),
  awful.button(
    { modkey }, 1,
    function(c)
      c:activate { context = "mouse_click", action = "mouse_move" }
    end
  ),
  awful.button(
    { modkey }, 3,
    function(c)
      c:activate { context = "mouse_click", action = "mouse_resize" }
    end
  )
}

M.mouse = {
  awful.button({}, 4, awful.tag.viewnext),
  awful.button({}, 5, awful.tag.viewprev)
}

M.titlebar = function(c)
  return {
    awful.button(
      {}, 1,
      function()
        c:activate { context = "titlebar", action = "mouse_move" }
      end
    ),
    awful.button(
      {}, 3,
      function()
        c:activate { context = "titlebar", action = "mouse_resize" }
      end
    )
  }
end

M.init()
return M
