local instances = require("panels.instances")

local M = {
  floating = {
    horizontal = require("panels.floating.horizontal"),
    vertical = require("panels.floating.vertical"),
  },
  offscreen = require("panels.offscreen"),

  get_by_name = function(s, name)
    local instance = instances[tostring(s) .. name]
    assert(instance ~= nil, "No panel named '" .. name .. "on " .. tostring(s) .. "'!")
    return instance
  end,
}

return M
