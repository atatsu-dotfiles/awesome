local floating = require("panels.floating._floating")

-- theme.panel_floating_vertical_width = dpi(20)
-- theme.panel_floating_vertical_margins = {
--   top = dpi(3),
--   bottom = dpi(3),
--   left = dpi(5),
--   right = dpi(5)
-- }
-- theme.panel_floating_vertical_placement = awful.placement.top
-- theme.panel_floating_vertical_height = nil

---Create a floating vertical panel.
return function(options)
  return floating.setup("vertical", options)
end
