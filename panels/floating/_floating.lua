local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

local helpers = require("helpers")
local instances = require("panels.instances")

local capi = {
  ---@diagnostic disable-next-line: undefined-global
  client = client,
  ---@diagnostic disable-next-line: undefined-global
  screen = screen,
  ---@diagnostic disable-next-line: undefined-global
  tag = tag,
}
local dpi = beautiful.xresources.apply_dpi

local default_conditional_create = function(s)
  return true
end


local default_margin_mapping = {
  horizontal = {
    top = dpi(3),
    bottom = dpi(3),
    left = dpi(5),
    right = dpi(5),
  },
  vertical = {
    top = dpi(5),
    bottom = dpi(5),
    left = dpi(3),
    right = dpi(3),
  }
}

local default_placement_mapping = {
  horizontal = awful.placement.top,
  vertical = awful.placement.left,
}

local extend_vertical_popup_args = function(orientation, s, options)
  if orientation ~= "vertical" then return {} end

  local panel_width = helpers.sanitize_numeric(
    options.width or beautiful.panel_floating_vertical_width,
    {
      min = dpi(1),
      max = s.geometry.width,
      default = dpi(20)
    }
  )
  return {
    maximum_width = panel_width,
    maximum_height = options.stretch and s.geometry.height or options.height or beautiful.panel_floating_vertical_height,
    minimum_height = options.stretch and s.geometry.height or options.height or beautiful.panel_floating_vertical_height,
  }
end

local extend_horizontal_popup_args = function(orientation, s, options)
  if orientation ~= "horizontal" then return {} end

  local panel_height = helpers.sanitize_numeric(
    options.height or beautiful.panel_floating_horizontal_height,
    {
      min = dpi(1),
      max = s.geometry.height,
      default = dpi(20)
    }
  )
  return {
    maximum_height = panel_height,
    maximum_width = options.stretch and s.geometry.width or options.width or beautiful.panel_floating_horizontal_width,
    minimum_width = options.stretch and s.geometry.width or options.width or beautiful.panel_floating_horizontal_width,
  }
end

---Show or hide the panel based on layout and/or client based criteria.
local show_hide_panel = function(panel, client_or_tag, options, screen)
  -- if no tag is selected and this is invoked we don't want to throw an error
  -- so return early
  if client_or_tag.client == nil and client_or_tag.tag == nil then return end
  if client_or_tag.tag ~= nil and client_or_tag.tag.screen ~= screen then return end
  if client_or_tag.client ~= nil and client_or_tag.client.screen ~= screen then return end

  -- If a client activity is causing this to fire we still need to inspect
  -- tag atttributes, so get that sorted
  local tag = client_or_tag.tag
  if tag == nil then
    tag = client_or_tag.client.first_tag
  end
  -- if a client went unmanaged it won't have a tag so we'll have
  -- to pull it from the screen
  if tag == nil then
    -- FIXME: This assertion needs additional checks, Predecessor triggers it when it is starting
    --        up... maybe it has a window that opens then closes too quickly to notice?
    -- assert(client_or_tag.client_destroyed == true, "Expected there to be a tag")
    tag = screen.selected_tag
  end
  -- if the selected tag's layout is in the list of layouts that the
  -- panel should be hidden on it doesn't matter whether or not there
  -- is a client positive condition
  for _, layout_name in ipairs(options.hide_on_layouts) do
    if tag.layout.name == layout_name then
      panel.visible = false
      return
    end
  end

  -- If we're operating on a tag we need to collect all clients,
  -- else just use the supplied one
  local clients = {}
  if client_or_tag.client ~= nil then
    clients[#clients + 1] = client_or_tag.client
  else
    clients = client_or_tag.tag:clients()
  end

  if client_or_tag.client_destroyed then
    assert(#clients == 1, "Expected there to be only 1 client")
  end

  -- now clients are free to affect the panel's visibility
  -- start with fullscreen
  if options.hide_on_client_fullscreen then
    local fullscreens = {}
    for _, client in ipairs(clients) do
      if client.screen ~= screen then goto continue end
      if client_or_tag.client_destroyed then goto continue end
      fullscreens[#fullscreens + 1] = client.fullscreen
      ::continue::
    end
    if helpers.any(fullscreens) then
      panel.visible = false
      return
    end
  end

  if options.hide_on_client_maximized then
    local maximized = {}
    for _, client in ipairs(clients) do
      if client.screen ~= screen then goto continue end
      if client_or_tag.client_destroyed then goto continue end
      maximized[#maximized + 1] = client.maximized
      ::continue::
    end
    if helpers.any(maximized) then
      panel.visible = false
      return
    end
  end

  panel.visible = true
end

local create_panel = function(orientation, s, widgets, options)
  local horizontal_args = extend_horizontal_popup_args(orientation, s, options)
  local vertical_args = extend_vertical_popup_args(orientation, s, options)

  local maximum_height = horizontal_args.maximum_height or vertical_args.maximum_height
  local minimum_height = horizontal_args.minimum_height or vertical_args.minimum_height
  local maximum_width = horizontal_args.maximum_width or vertical_args.maximum_width
  local minimum_width = horizontal_args.minimum_width or vertical_args.minimum_width

  local panel = awful.popup {
    -- NOTE:
    -- in order for all elements to be returned the `unpack`
    -- call must be the last expression
    -- http://www.lua.org/pil/5.1.html
    -- table.unpack(widgets),
    widget = {
      widget = wibox.container.margin,
      margins = options.margins or beautiful["panel_floating_" .. orientation .. "_margins"] or
          default_margin_mapping[orientation],
      {
        layout = wibox.layout.align[orientation],
        expand = "none",
        { layout = widgets.first.layout or wibox.layout.fixed[orientation],  table.unpack(widgets.first) },
        { layout = widgets.second.layout or wibox.layout.fixed[orientation], table.unpack(widgets.second) },
        { layout = widgets.third.layout or wibox.layout.fixed[orientation],  table.unpack(widgets.third) },
      }
    },
    screen = s,
    type = "dock",
    placement = options.placement or beautiful["panel_floating_" .. orientation .. "_placement"] or
        default_placement_mapping[orientation],
    shape = function(cr, width, height)
      -- Don't round the corners if the panel is consuming the entire space
      -- of the screen so that it looks flush with the screen's edges.
      if options.stretch then
        gears.shape.rectangle(cr, width, height)
        return
      end
      gears.shape.rounded_rect(cr, width, height, dpi(6))
    end,
    visible = true,
    -- FIXME: Use `beautiful.bg_normal`
    bg = beautiful.colors.dark_grey,
    maximum_height = maximum_height,
    minimum_height = minimum_height,
    maximum_width = maximum_width,
    minimum_width = minimum_width,
    ontop = options.ontop,
  }

  if options.restrict_workarea then
    local edge = helpers.find_nearest_edge(panel:geometry(), s)
    local is_horizontal = orientation == "horizontal"
    local is_top = is_horizontal and string.find(edge, "top")
    local is_bottom = is_horizontal and string.find(edge, "bottom")
    local is_left = not is_horizontal and string.find(edge, "left")
    local is_right = not is_horizontal and string.find(edge, "right")
    local width = panel.maximum_width
    local height = panel.maximum_height
    local struts = {
      top = is_top and height or nil,
      bottom = is_bottom and height or nil,
      left = is_left and width or nil,
      right = is_right and width or nil,
    }
    panel:struts(struts)
  end

  if helpers.any({
        options.hide_on_client_maximized,
        options.hide_on_client_fullscreen,
        options.hide_on_layouts
      }) then
    if options.hide_on_client_maximized or options.hide_on_client_fullscreen then
      capi.client.connect_signal("request::unmanage", function(c, context)
        show_hide_panel(panel, { client = c, client_destroyed = true }, options, s)
      end)
    end
    if options.hide_on_client_maximized then
      capi.client.connect_signal("property::maximized", function(c)
        show_hide_panel(panel, { client = c }, options, s)
      end)
    end
    if options.hide_on_client_fullscreen then
      capi.client.connect_signal("property::fullscreen", function(c)
        show_hide_panel(panel, { client = c }, options, s)
      end)
    end
    capi.tag.connect_signal("property::selected", function(tag)
      show_hide_panel(panel, { tag = tag }, options, s)
    end)
    if #options.hide_on_layouts > 0 then
      capi.tag.connect_signal("property::layout", function(tag)
        show_hide_panel(panel, { tag = tag }, options, s)
      end)
      show_hide_panel(panel, { tag = s.selected_tag }, options, s)
    end
  end

  return panel
end

local collect = function(s, widgets)
  local collected = {}
  for k, v in pairs(widgets) do
    if type(k) == "number" and type(v) == "function" then
      v = v(s)
    end
    collected[k] = v
  end
  return collected
end

local M = {}

---Connects the appropriate signal to create either a
---horizontal or vertical panel.
---
---`options` are:
---  `widgets`: An array of widgets to place in the panel.
---  These can be actual widgets or functions that return a widget.
---  `name`:
---  `margins`: A table of margin values for "left", "top", "right", "bottom".
---  `placement`: Placement function.
---  `height`:
---  `width`:
---  `stretch`: Fit the screen's dimensions. Default: `false`
---  `ontop`: Keep the panel on top of other windows. Default: `true`
---  `restrict_workarea`: Panel restricts the work area. Default: `false`
---  `created_callback`: Function to execute after the panel is created.
---  `hide_on_client_maximized`: Whether the panel should be hidden when
---  clients are maximized. Default: `false`
---  `hide_on_client_fullscreen`: Whether the panel should be hidden when
---  clients are fullscreened. Default: `true`
---  `hide_on_layouts`: List of layout names that if active should hide the panel.
---  `conditional_create`:
---@param orientation '"horizontal"' | '"vertical"' Specify the direction of the panel
---@param options? table Options to control panel contents and behavior
M.setup = function(orientation, options)
  if orientation ~= "horizontal" and orientation ~= "vertical" then
    error("Expected \"horizontal\" or \"vertical\"")
  end

  options = options or {}
  options.ontop = options.ontop == nil and true or options.ontop
  options.stretch = options.stretch ~= nil and options.stretch or false
  options.restrict_workarea = options.restrict_workarea ~= nil and options.restrict_workarea or false
  options.hide_on_client_fullscreen = options.hide_on_client_fullscreen == nil and true or
      options.hide_on_client_fullscreen
  options.hide_on_client_maximized = options.hide_on_client_maximized ~= nil and options.hide_on_client_maximized or
      false
  options.hide_on_layouts = options.hide_on_layouts ~= nil and options.hide_on_layouts or {}
  local widgets = options.widgets or {}
  local conditional_create = options.conditional_create or default_conditional_create

  local s = options.screen
  assert(s ~= nil, "panel requires a screen")

  -- TODO: `conditional_create` could use a better name (if it sticks around that is)
  if not conditional_create(s) then return end

  local collected = {
    first = collect(s, widgets.left or widgets.first or widgets.top or {}),
    second = collect(s, widgets.middle or widgets.second or widgets),
    third = collect(s, widgets.right or widgets.third or widgets.bottom or {}),
  }

  local panel = create_panel(orientation, s, collected, options)
  if options.name ~= nil then
    instances[tostring(s) .. options.name] = panel
  else
    instances[#instances + 1] = panel
  end

  if type(options.created_callback) == "function" then
    options.created_callback(panel)
  end
end

return M
