local floating = require("panels.floating._floating")

-- theme.panel_floating_horizontal_height = dpi(20)
-- theme.panel_floating_horizontal_margins = {
--   top = dpi(3),
--   bottom = dpi(3),
--   left = dpi(5),
--   right = dpi(5)
-- }
-- theme.panel_floating_horizontal_placement = awful.placement.top
-- theme.panel_floating_horizontal_width = nil
-- local M = {}

---Create a floating horizontal panel.
return function(options)
  return floating.setup("horizontal", options)
end
