-- theme.offscreen_panel_height = dpi(20)
-- theme.offscreen_panel_margins = {
--   top = dpi(3),
--   bottom = dpi(3),
--   left = dpi(5),
--   right = dpi(5)
-- }
-- theme.offscreen_panel_placement = awful.placement.top
-- theme.offscreen_panel_width = nil

local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

local rubato = require("lib.rubato")

local helpers = require("helpers")
local prefs = require("prefs")

local capi = {
  awesome = awesome,
  screen = screen,
}
local dpi = beautiful.xresources.apply_dpi

local default_panel_margins = {
  top = dpi(3),
  bottom = dpi(3),
  left = dpi(5),
  right = dpi(5)
}

local find_nearest_edge = function(geo, s)
  local _, edge = awful.placement.closest_corner(
    { coords = function() return { x = geo.x, y = geo.y } end },
    { include_sides = true, parent = s }
  )
  return edge
end

local create_panel = function(s, widgets, options)
  local children = {
    layout = wibox.layout.fixed.horizontal,
    -- NOTE:
    -- in order for all elements to be returned the `unpack`
    -- call must be the last expression
    -- http://www.lua.org/pil/5.1.html
    table.unpack(widgets)
  }

  local panel_height = helpers.sanitize_numeric(
    options.height or beautiful.offscreen_panel_height,
    {
      min = dpi(1),
      max = s.geometry.height,
      default = dpi(20)
    }
  )
  local panel = awful.popup {
    widget = {
      widget = wibox.container.margin,
      margins = options.margins or beautiful.offscreen_panel_margins or default_panel_margins,
      children
    },
    maximum_height = panel_height,
    maximum_width = options.width or beautiful.offscreen_panel_width,
    minimum_width = options.width or beautiful.offscreen_panel_width,
    screen = s,
    type = "dock",
    placement = options.placement or beautiful.offscreen_panel_placement or awful.placement.top,
    shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, dpi(6))
    end,
    visible = true,
    -- FIXME: Use `beautiful.bg_normal`
    bg = beautiful.colors.dark_grey
  }

  local coords = {
    desired_y = nil,
    offscreen_y = nil,
  }
  local animation

  -- panel:connect_signal("property::x", function(_panel)
  --   print("x: ", _panel.x)
  -- end)
  local y_signal_handler
  local setting_up = false
  y_signal_handler = function(_panel)
    if animation or setting_up then return end
    setting_up = true

    local edge = find_nearest_edge(
      { x = _panel.x, y = _panel.y },
      s
    )
    if not string.find(edge, "top") and not string.find(edge, "bottom") then
      _panel:disconnect_signal("property::y", y_signal_handler)
      return
    end

    coords.desired_y = _panel.y
    coords.offscreen_y = _panel.y - _panel.y - _panel.height
    animation = rubato.timed {
      duration = 0.3,
      intro = 0.1,
      pos = coords.offscreen_y,
      easing = rubato.quadratic,
      subscribed = function(y)
        _panel.y = y
      end
    }
    _panel:disconnect_signal("property::y", y_signal_handler)
    _panel.y = coords.offscreen_y
    -- even though the `y` coord is being set here something is still causing
    -- it to finalize in it's onscreen position so a hack for now to make sure
    -- it isn't visible
    _panel.visible = false
  end
  -- panel:connect_signal("property::y", y_signal_handler)

  --capi.awesome.connect_signal("panel::visible", function(visible)
  s:connect_signal("panel::visible", function(_, visible)
    print("panel::visible", visible)
    if not animation then return end
    panel.visible = true
    if animation.running then animation:abort() end

    if visible then
      assert(coords.desired_y ~= nil, "no desired_y coord!")
      animation.target = coords.desired_y
      print("set to desired")
      return
    end
    assert(coords.offscreen_y ~= nil, "no offscreen_y coord!")
    animation.target = coords.offscreen_y
    print("set to offscreen")
  end)

  -- panel:struts({
  --   top = height
  -- })

  return panel
end

local modkey = prefs.modkey
-- TODO: Need to determine which mod key is being used so
--       that the appropriate named keys can be used below
--       (static "Super_X" could instead be "Alt_X", for example)
local keys = {
  -- There is an issue capturing the keyup/keydown of modifier keys
  -- see: https://github.com/awesomeWM/awesome/issues/169
  -- The below works around it
  awful.key {
    modifiers = {},
    key = "Super_L",
    on_press = function()
      awful.screen.focused():emit_signal("panel::visible", true)
    end,
  },
  awful.key {
    modifiers = { modkey },
    key = "Super_L",
    on_release = function()
      awful.screen.focused():emit_signal("panel::visible", false)
    end,
  },
  awful.key {
    modifiers = {},
    key = "Super_R",
    on_press = function()
      awful.screen.focused():emit_signal("panel::visible", true)
    end,
  },
  awful.key {
    modifiers = { modkey },
    key = "Super_R",
    on_release = function()
      awful.screen.focused():emit_signal("panel::visible", false)
    end,
  }
}

local M = {}

local bindings_added = false

-- options
-- options.widgets
-- options.height
-- options.margins
-- options.placement
-- options.width
M.setup = function(options)
  local widgets = options.widgets or {}

  if not bindings_added then
    -- awful.keyboard.append_global_keybindings(keys)
    bindings_added = true
  end

  capi.screen.connect_signal("request::desktop_decoration", function(s)
    local collected = {}
    for _, widget in ipairs(widgets) do
      if type(widget) == "function" then
        widget = widget(s)
      end
      collected[#collected + 1] = widget
    end
    return create_panel(s, collected, options)
  end)
end

return M
