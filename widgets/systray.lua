local wibox = require("wibox")

-- The docs say that only one systray can exist.
-- Ensure that is the case, while flipping its
-- horizontal status when necessary.
local _systray = nil

local M = {}

function M.horizontal()
  if _systray == nil then
    _systray = wibox.widget.systray()
  end
  _systray.horizontal = true
  return _systray
end

function M.vertical()
  if _systray == nil then
    _systray = wibox.widget.systray()
  end
  _systray.horizontal = false
  return _systray
end

return M
