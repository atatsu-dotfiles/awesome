local M = {
  clock = require("widgets.clock"),
  layout = require("widgets.layout"),
  taglist = require("widgets.taglist"),
  systray = require("widgets.systray"),
}

return M
