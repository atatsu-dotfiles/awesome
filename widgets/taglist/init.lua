local M = {
  horizontal = require("widgets.taglist.horizontal"),
  vertical = require("widgets.taglist.vertical"),
}

return M
