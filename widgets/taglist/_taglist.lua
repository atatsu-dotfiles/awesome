local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

local bling = require("lib.bling")

local helpers = require("helpers")
local prefs = require("prefs")

local capi = {
  ---@diagnostic disable-next-line: undefined-global
  awesome = awesome,
  ---@diagnostic disable-next-line: undefined-global
  client = client,
  ---@diagnostic disable-next-line: undefined-global
  mouse = mouse,
}
local dpi = beautiful.xresources.apply_dpi
local modkey = prefs.modkey

local default_tag_preview_config = {
  show_client_content = true,
  honor_padding = true,
  honor_workarea = true,
  placement_fn = function(preview_widget)
    local edge = helpers.find_nearest_edge(capi.mouse.coords(), capi.mouse.screen)
    -- The preview popup is aligned with its `edge` up against the mouse.
    -- Initially I had the impression that using `awful.placement.right`, for example,
    -- would place the preview on the right side of the cursor. However, in reality
    -- the preview window's _right_ edge was up against the cursor.
    local placement = awful.placement[edge]
    local margins = {}
    local spacing = dpi(20)
    if string.find(edge, "left") then
      margins.left = spacing
    elseif string.find(edge, "right") then
      margins.right = spacing
    elseif string.find(edge, "top") then
      margins.top = spacing
    elseif string.find(edge, "bottom") then
      margins.bottom = spacing
    end
    placement(preview_widget, {
      margins = margins,
      parent = capi.mouse,
    })
  end,
  background_widget = wibox.widget {
    image = beautiful.wallpaper,
    horizontal_fit_policy = "fit",
    vertical_fit_policy = "fit",
    widget = wibox.widget.imagebox
  }
}

local default_buttons = {
  awful.button({}, 1, function(t) t:view_only() end),
  awful.button(
    { modkey }, 1,
    function(t)
      if capi.client.focus then
        capi.client.focus:move_to_tag(t)
      end
    end
  ),
  awful.button({}, 3, awful.tag.viewtoggle),
  awful.button(
    { modkey }, 3,
    function(t)
      if capi.client.focus then
        capi.client.focus:toggle_tag(t)
      end
    end
  ),
  awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
  awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
}

local mouse_enter = function(tag, s)
  return function()
    if #tag:clients() > 0 and not tag.selected then
      capi.awesome.emit_signal("bling::tag_preview::update", tag)
      capi.awesome.emit_signal("bling::tag_preview::visibility", s, true)
    end
  end
end

local mouse_leave = function(s)
  return function()
    capi.awesome.emit_signal("bling::tag_preview::visibility", s, false)
  end
end

local M = {}

---Create a taglist.
---
---`options` are:
---  `buttons`: An array of `awful.button`s that the taglist should use
---  `tag_count`:
---  `tag_preview_config`: A table of configuration options for `bling.widget.tag_preview`
---
--- Theme options are:
---  `theme.taglist_icon_color_active`:
---  `theme.taglist_icon_color_normal`:
---  `theme.taglist_icon_color_occupied`:
---@param orientation '"horizontal"' | '"vertical"' Specify the direction of the taglist
---@param options? table Options to control taglist behavior
M.setup = function(orientation, options)
  if orientation ~= "horizontal" and orientation ~= "vertical" then
    error("Expected \"horizontal\" or \"vertical\"")
  end

  options = options or {}

  local tag_preview_config = options.tag_preview_config or default_tag_preview_config
  bling.widget.tag_preview.enable(tag_preview_config)

  local buttons = options.buttons or default_buttons

  return function(s)
    if options.screen ~= nil then
      s = options.screen
    end

    local taglist = awful.widget.taglist {
      screen = s,
      filter = awful.widget.taglist.filter.all,
      base_layout = wibox.layout.fixed[orientation],
      buttons = buttons,
      style = {
        shape = gears.shape.rounded_rect,
        -- TODO: options and theme override
        spacing = dpi(6),
      },

      widget_template = {
        id = "background_role",
        -- this is the "self" arg in the callbacks
        widget = wibox.container.background,

        create_callback = function(self, tag)
          self:connect_signal("mouse::enter", mouse_enter(tag, s))
          self:connect_signal("mouse::leave", mouse_leave(s))
          -- hide the preview whenever a tag is selected
          tag:connect_signal("property::selected", function()
            -- Prevent needless signal emits. This will execute for every single tag whenever
            -- a tag selection changes (once for the tag actually being selected, and once
            -- for every other tag when their `selected` is set to `false`).
            if not tag.selected then return end
            capi.awesome.emit_signal("bling::tag_preview::visibility", s, false)
          end)

          if tag.selected then
            self:get_children_by_id("tag_icon")[1].bg = beautiful.taglist_icon_color_active
            return
          end

          if #tag:clients() > 0 then
            self:get_children_by_id("tag_icon")[1].bg = beautiful.taglist_icon_color_occupied
            return
          end
        end,
        update_callback = function(self, tag)
          if tag.selected then
            self:get_children_by_id("tag_icon")[1].bg = beautiful.taglist_icon_color_active
            return
          end

          if #tag:clients() > 0 then
            self:get_children_by_id("tag_icon")[1].bg = beautiful.taglist_icon_color_occupied
            return
          end

          self:get_children_by_id("tag_icon")[1].bg = beautiful.taglist_icon_color_normal
        end,

        {
          widget = wibox.container.background,
          -- TODO: options and theme override
          forced_width = dpi(20),
          forced_height = dpi(20),
          shape = gears.shape.circle,

          {
            layout = wibox.layout.align[orientation],

            {
              widget = wibox.container.margin,
              -- TODO: options and theme override
              left = orientation == "horizontal" and dpi(6) or 0,
              top = orientation == "vertical" and dpi(6) or 0,
            },
            {
              id = "tag_icon",
              widget = wibox.container.background,
              shape = gears.shape.circle,
              bg = beautiful.taglist_icon_color_normal,
            },
            {
              widget = wibox.container.margin,
              -- TODO: options and theme override
              right = orientation == "horizontal" and dpi(6) or 0,
              bottom = orientation == "vertical" and dpi(6) or 0,
            }
          }
        }
      }
    }

    return taglist
  end
end

return M
