local taglist = require("widgets.taglist._taglist")

return function(options)
  return taglist.setup("vertical", options)
end
