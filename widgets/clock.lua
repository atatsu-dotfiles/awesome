local beautiful = require("beautiful")
local wibox = require("wibox")

local dpi = beautiful.xresources.apply_dpi

local M = {}

local horizontal_clock

function M.horizontal()
  if horizontal_clock ~= nil then return horizontal_clock end
  horizontal_clock = wibox.widget.textclock()
  return horizontal_clock
end

local vertical_clock

-- FIXME: Use themable colors rather than specifying them concretely here
--        For example, `clock_text_color` and `clock_separator_color`.
function M.vertical()
  if vertical_clock ~= nil then return vertical_clock end
  vertical_clock = wibox.widget {
    layout = wibox.layout.fixed.vertical,
    {
      widget = wibox.widget.textclock,
      format = "<b><span foreground=\"" .. beautiful.colors.blue .. "\">%H</span></b>",
      halign = "center",
    },
    {
      widget = wibox.widget.textclock,
      format = "<b><span foreground=\"" .. beautiful.colors.blue .. "\">%M</span></b>",
      halign = "center",
    },
    {
      widget = wibox.widget.separator,
      orientation = "horizontal",
      forced_height = dpi(1),
      color = beautiful.colors.pink,
    },
    {
      widget = wibox.widget.textclock,
      format = "<b><span foreground=\"" .. beautiful.colors.blue .. "\">%m</span></b>",
      halign = "center",
    },
    {
      widget = wibox.widget.textclock,
      format = "<b><span foreground=\"" .. beautiful.colors.blue .. "\">%d</span></b>",
      halign = "center",
    }
  }
  return vertical_clock
end

return M
