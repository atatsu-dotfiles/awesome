local awful = require("awful")

local default_buttons = {
  awful.button({}, 1, function() awful.layout.inc(1) end),
  awful.button({}, 3, function() awful.layout.inc(-1) end),
  awful.button({}, 4, function() awful.layout.inc(1) end),
  awful.button({}, 5, function() awful.layout.inc(-1) end)
}

-- options
-- options.buttons
local layout = function(options)
  options = options or {}
  local buttons = options.buttons or default_buttons

  return function(s)
    s = options.screen or s
    return awful.widget.layoutbox {
      screen = s,
      buttons = buttons
    }
  end
end

return layout
