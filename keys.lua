local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup").widget

local prefs = require("prefs")

local capi = {
  ---@diagnostic disable-next-line: undefined-global
  awesome = awesome,
  ---@diagnostic disable-next-line: undefined-global
  client = client,
  ---@diagnostic disable-next-line: undefined-global
  mouse = mouse,
}

local ctl = "Control"
local shift = "Shift"
local is_setup = false

local M
M = {
  init = function()
    if is_setup then return end
    is_setup = true

    for _, bindings in pairs(M.global) do
      awful.keyboard.append_global_keybindings(bindings)
    end
  end
}

local modkey = prefs.modkey

M.client = {
  awful.key(
    { modkey }, "f",
    function(c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end,
    { description = "toggle fullscreen", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "c",
    function(c) c:kill() end,
    { description = "close", group = "client" }
  ),
  awful.key(
    { modkey, ctl }, "space",
    awful.client.floating.toggle,
    { description = "toggle floating", group = "client" }
  ),
  awful.key(
    { modkey, ctl }, "Return",
    function(c) c:swap(awful.client.getmaster()) end,
    { description = "move to master", group = "client" }
  ),
  awful.key(
    { modkey }, "o",
    function(c) c:move_to_screen() end,
    { description = "move to next screen", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "o",
    function(c) c:move_to_screen(c.screen.index - 1) end,
    { description = "move to previous screen", group = "client" }
  ),
  awful.key(
    { modkey }, "t",
    awful.titlebar.toggle,
    { description = "toggle titlebar", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "t",
    function(c) c.ontop = not c.ontop end,
    { description = "toggle keep on top", group = "client" }
  ),
  -- TODO: After minimizing auto focus next available client
  awful.key(
    { modkey, shift }, "n",
    function(c) c.minimized = true end,
    { description = "minimize", group = "client" }
  ),
  awful.key(
    { modkey }, "m",
    function(c)
      c.maximized = not c.maximized
      c:raise()
    end,
    { description = "toggle maximized", group = "client" }
  ),
  awful.key(
    { modkey, ctl }, "m",
    function(c)
      c.maximized_vertical = not c.maximized_vertical
      c:raise()
    end,
    { description = "toggle vertically maximized", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "m",
    function(c)
      c.maximized_horizontal = not c.maximized_horizontal
      c:raise()
    end,
    { description = "toggle horizontally maximized", group = "client" }
  )
}

M.global = {}

M.global.awesome = {
  awful.key(
    { modkey }, "s",
    hotkeys_popup.show_help,
    { description = "show key binds", group = "awesome" }
  ),
  awful.key(
    { modkey, ctl }, "r",
    capi.awesome.restart,
    { description = "reload", group = "awesome" }
  ),
  awful.key(
    { modkey, shift }, "q",
    capi.awesome.quit,
    { description = "quit", group = "awesome" }
  )
}

---@alias Direction
---| '"left"'
---| '"right"'
---| '"up"'
---| '"down"'

---If there is only one client on the screen focus it,
---otherwise focus in the supplied direction.
---It seems as though awesome needs a little help when a screen
---is reduced to only one client in that `focus.byidx` has no
---effect (the singular client does not gain focus).
---So instead check for the number of clients on the current
---screen and if there is only one give it immediate focus.
---Using `focus.bydirection` now, and though I haven't tested if
---it suffers from the same thing as `focus.byidx` this check isn't
---hurting anything so I've left it.
---@param direction Direction
---@return function
local focus_by_direction = function(direction)
  return function()
    local s = awful.screen.focused()
    if #s.clients == 1 then
      s.clients[1]:activate { context = "key.focus" }
      return
    end
    awful.client.focus.bydirection(direction)
    -- move the mouse cursor to the newly focused client
    -- mid point of client (y), wedged in a tad (x) to make sure
    -- any mouse movements (like scrolling) can be picked up immediately
    -- TODO: might be worthwhile to take useless_gap into account
    local c = capi.client.focus
    if not c then return end
    capi.mouse.coords { x = c.x + 10, y = c.y + c.height / 2 }
  end
end

M.global.directional_movement = {
  awful.key(
    { modkey }, "j",
    focus_by_direction("down"),
    { description = "focus next down", group = "client" }
  ),
  awful.key(
    { modkey }, "k",
    focus_by_direction("up"),
    { description = "focus next up", group = "client" }
  ),
  awful.key(
    { modkey }, "h",
    focus_by_direction("left"),
    { description = "focus next left", group = "client" }
  ),
  awful.key(
    { modkey }, "l",
    focus_by_direction("right"),
    { description = "focus next right", group = "client" }
  ),
}

M.global.directional_swapping = {
  awful.key(
    { modkey, shift }, "j",
    function() awful.client.swap.bydirection("down") end,
    { description = "swap with client down", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "k",
    function() awful.client.swap.bydirection("up") end,
    { description = "swap with client up", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "h",
    function() awful.client.swap.bydirection("left") end,
    { description = "swap with client left", group = "client" }
  ),
  awful.key(
    { modkey, shift }, "l",
    function() awful.client.swap.bydirection("right") end,
    { description = "swap with client right", group = "client" }
  ),
}

M.global.client = {
  awful.key(
    { modkey }, "u",
    awful.client.urgent.jumpto,
    { description = "jump to urgent", group = "client" }
  ),
  awful.key(
    { modkey }, "Tab",
    function()
      awful.client.focus.history.previous()
      if capi.client.focus then
        capi.client.focus:raise()
      end
    end,
    { description = "go back", group = "client" }
  ),
  awful.key(
    { modkey, ctl }, "n",
    function()
      local c = awful.client.restore()
      if c then
        c:activate { raise = true, context = "key.unminimize" }
      end
    end,
    { description = "restore minimized", group = "client" }
  ),
}

M.global.launcher = {
  awful.key(
    { modkey }, "Return",
    function() awful.spawn(prefs.terminal) end,
    { description = "open a terminal", group = "launcher" }
  ),
  awful.key(
    { modkey }, "d",
    function() awful.spawn.with_shell(prefs.launcher) end,
    { description = "application launcher", group = "launcher" }
  )
}

M.global.layout = {
  awful.key(
    { modkey, shift }, "=",
    awful.tag.togglemfpol,
    { description = "toggle size fill policy", group = "layout" }
  ),
  awful.key(
    { modkey, ctl }, "l",
    function() awful.tag.incmwfact(0.05) end,
    { description = "increase master width factor", group = "layout" }
  ),
  awful.key(
    { modkey, ctl }, "h",
    function() awful.tag.incmwfact(-0.05) end,
    { description = "decrease master width factor", group = "layout" }
  ),
  awful.key(
    { modkey }, "=",
    function() awful.tag.incnmaster(1, nil, false) end,
    { description = "increase number of master clients", group = "layout" }
  ),
  awful.key(
    { modkey, ctl }, "=",
    function() awful.tag.incnmaster(-1, nil, false) end,
    { description = "decrease number of master clients", group = "layout" }
  ),
  awful.key(
    { modkey }, "-",
    function() awful.tag.incncol(1, nil, true) end,
    { description = "increase the number of columns", group = "layout" }
  ),
  awful.key(
    { modkey, ctl }, "-",
    function() awful.tag.incncol(-1, nil, true) end,
    { description = "decrease the number of columns", group = "layout" }
  ),
  -- TODO:
  -- awful.key(
  --   { modkey, shift }, "g",
  --   function() awful.tag.incgap(1) end,
  --   { description = "increase spacing between clients", group = "layout" }
  -- ),
  -- TODO:
  -- awful.key(
  --   { modkey, ctl }, "g",
  --   function() awful.tag.incgap(-1) end,
  --   { description = "decrease spacing between clients", group = "layout" }
  -- ),
  awful.key(
    { modkey }, "space",
    function() awful.layout.inc(1) end,
    { description = "select next", group = "layout" }
  ),
  awful.key(
    { modkey, shift }, "space",
    function() awful.layout.inc(-1) end,
    { description = "select previous", group = "layout" }
  ),
}

M.global.screen = {
  awful.key(
    { modkey, ctl }, "j",
    function() awful.screen.focus_relative(1) end,
    { description = "focus next screen", group = "screen" }
  ),
  awful.key(
    { modkey, ctl }, "k",
    function() awful.screen.focus_relative(-1) end,
    { description = "focus previous screen", group = "screen" }
  )
}

M.global.tag = {
  awful.key(
    { modkey }, "p",
    awful.tag.viewprev,
    { description = "view previous", group = "tag" }
  ),
  awful.key(
    { modkey }, "n",
    awful.tag.viewnext,
    { description = "view next", group = "tag" }
  ),
  awful.key(
    { modkey }, "Escape",
    awful.tag.history.restore,
    { description = "go back", group = "tag" }
  ),
  awful.key {
    modifiers = { modkey },
    keygroup = "numrow",
    description = "view only",
    group = "tag",
    on_press = function(index)
      local screen = awful.screen.focused()
      local tag = screen.tags[index]
      if tag then tag:view_only() end
    end
  },
  awful.key {
    modifiers = { modkey, ctl },
    keygroup = "numrow",
    description = "toggle",
    group = "tag",
    on_press = function(index)
      local screen = awful.screen.focused()
      local tag = screen.tags[index]
      if tag then awful.tag.viewtoggle(tag) end
    end
  },
  awful.key {
    modifiers = { modkey, shift },
    keygroup = "numrow",
    description = "move focused client to",
    group = "tag",
    on_press = function(index)
      if not capi.client.focus then return end
      local tag = capi.client.focus.screen.tags[index]
      if tag then capi.client.focus:move_to_tag(tag) end
    end
  },
  awful.key {
    modifiers = { modkey, ctl, shift },
    keygroup = "numrow",
    description = "toggle focused client on",
    group = "tag",
    on_press = function(index)
      if not capi.client.focus then return end
      local tag = capi.client.focus.screen.tags[index]
      if tag then capi.client.focus:toggle_tag(tag) end
    end
  },
}

M.init()
return M
