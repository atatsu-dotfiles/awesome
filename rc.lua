require("awful.autofocus")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")

local helpers = require("helpers")


local dpi = beautiful.xresources.apply_dpi
local capi = {
  ---@diagnostic disable-next-line: undefined-global
  awesome = awesome,
  ---@diagnostic disable-next-line: undefined-global
  screen = screen,
  ---@diagnostic disable-next-line: undefined-global
  tag = tag,
}

local theme_dir = gears.filesystem.get_configuration_dir() .. "theme/"
beautiful.init(theme_dir .. "theme.lua")

local bling = require("lib.bling")
-- bling.module.flash_focus.enable()

require("buttons")
require("keys")
require("rules")
require("signals")

local panels = require("panels")
local widgets = require("widgets")

capi.tag.connect_signal("request::default_layouts", function()
  awful.layout.append_default_layouts({
    bling.layout.centered,
    bling.layout.equalarea,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.right,
    bling.layout.mstab,
    awful.layout.suit.floating,
  })
end)

capi.screen.connect_signal("request::desktop_decoration", function(s)
  for i = 1, 5 do
    local selected = i == 1
    awful.tag.add(tostring(i), {
      layout = awful.layout.layouts[1],
      screen = s,
      selected = selected
    })
  end

  -- {{{ Single panel for tiled layouts
  panels.floating.vertical {
    name = "main",
    screen = s,
    widgets = {
      top = { widgets.layout { screen = s } },
      middle = { widgets.taglist.vertical { screen = s } },
      bottom = { widgets.systray.vertical(), widgets.clock.vertical() },
    },
    placement = awful.placement.left,
    stretch = true,
    restrict_workarea = true,
    hide_on_layouts = { "floating" },
  }
  -- }}}
  --
  -- {{{ Split panels for floating layout
  panels.floating.vertical {
    name = "middle",
    screen = s,
    widgets = { widgets.taglist.vertical { screen = s } },
    placement = function(panel)
      awful.placement.left(panel, { margins = { left = dpi(10) } })
    end,
    hide_on_client_maximized = true,
    hide_on_layouts = { "centered", "mstab", "equalarea", "tile", "tileleft" },
  }
  panels.floating.vertical {
    name = "top",
    screen = s,
    widgets = { widgets.layout { screen = s } },
    placement = function(panel)
      local middle = panels.get_by_name(panel.screen, "middle")
      awful.placement.next_to(
        panel,
        {
          geometry = middle,
          preferred_positions = "top",
          preferred_anchors = "middle",
          margins = { bottom = dpi(panel.screen.geometry.height * .25) }
        }
      )
    end,
    hide_on_client_maximized = true,
    hide_on_layouts = { "centered", "mstab", "equalarea", "tile", "tileleft" },
  }
  panels.floating.vertical {
    name = "bottom",
    screen = s,
    widgets = { widgets.systray.vertical(), widgets.clock.vertical() },
    placement = function(panel)
      local middle = panels.get_by_name(panel.screen, "middle")
      awful.placement.next_to(
        panel,
        {
          geometry = middle,
          preferred_positions = "bottom",
          preferred_anchors = "middle",
          margins = { top = dpi(panel.screen.geometry.height * .25) }
        }
      )
    end,
    hide_on_client_maximized = true,
    hide_on_layouts = { "centered", "mstab", "equalarea", "tile", "tileleft" },
  }
  -- }}}
end)



if capi.awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Errors during startup",
    text = capi.awesome.startup_errors
  })
end
