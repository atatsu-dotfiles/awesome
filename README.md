## Dependencies

* [xcolor](https://github.com/Soft/xcolor)
* imagemagick
* rofi

## Submodules
* [bling](https://github.com/BlingCorp/bling)
* [rubato](https://github.com/andOrlando/rubato.git)

## TODO
* if ultrawide place wibar on vertical screen edge
* if not ultrawide place wibar on horizontal screen edge
* tasklist showing only icons
  - vertical and horizontal
* [layout-machi](https://github.com/xinhaoyuan/layout-machi)
* fancy floating volume display
  - only show when volume is adjusted
* clock widget designed for vertial wibar
  - at rest is just an icon
  - mouse hover or keybind lets it "roll out" to display values
* calendar widget [builtin](https://awesomewm.org/apidoc/popups_and_bars/awful.widget.calendar_popup.html)
* rebind directional movement / directional swapping keys depending on layout
  - bling mstab layout doesn't like `awful.client.focus.bydirection`,
    needs to be `awful.client.focus.byidx`
* directional swapping mouse follows client
  - same with move to master
* when manually toggling master fill policy it may be nice if spawning new clients
  didn't automatically set the master fill policy
  - not sure how feasible this is
