local M = {}
M.DIALOG = "dialog"
M.EXPAND = "expand"
M.MASTER_WIDTH_FACTOR = "master_width_factor"
return M
