local M
M = {
  modkey = "Mod4",

  terminal = "alacritty",
  launcher = "rofi -show combi -modes combi -combi-modes \"window,drun,run\" -theme arthur.rasi",
  -- any screen width beyond this should be treated as ultrawide
  ultrawide_threshold = 3440,
  focus_highlight_fade = 0.5,
}

return M
