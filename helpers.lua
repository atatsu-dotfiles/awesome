local awful = require("awful")

local prefs = require("prefs")

local M = {}

local pad = function(count)
  if count > 99 then count = 99 end
  if count < 0 then count = 0 end
  if count == 0 then return "" end
  return string.format("%" .. count .. "s", " ")
end


---Determine whether any of the items in the supplied table
---are truthy.
---Note: `nil` table values are not taken into account.
function M.any(t)
  for _, v in pairs(t) do
    if type(v) == "table" then
      if M.any(v) then return true end
    else
      if v then return true end
    end
  end
  return false
end

---Determine whether all of the items in the supplied table
---are truthy.
---
---Note: `nil` table values are not taken into account.
function M.all(t)
  for _, v in pairs(t) do
    if type(v) == "table" then
      if not M.all(v) then return false end
    else
      if not v then return false end
    end
  end
  return true
end

-- FIXME: One of the awesome libs (gears maybe?) has this already
--
--- Able to format a table's keys and values (recursively) as
--- a single string. Useful for printing to console.
--- (also, this is really gross)
---
--- object: the object to print
--- max_depth: maximum depth to traverse into
function M.tostring(object, max_depth)
  local indent = 0
  local current_depth = 1

  local _tostring
  _tostring = function(t, current_indent)
    if type(t) ~= "table" then
      return tostring(t)
    end

    local output = "{"
    current_indent = current_indent + 2
    local padding = pad(current_indent)
    local has_children = false

    for k, v in pairs(t) do
      local _k = k

      if type(k) ~= "number" then
        if type(k) ~= "string" then
          k = "<" .. tostring(k) .. ">"
        else
          k = '"' .. k .. '"'
        end
      end

      if not has_children then output = output .. "\n" end

      output = output .. padding .. "[" .. k .. "]"
      if max_depth == nil or current_depth < max_depth then
        output = output .. _tostring(v, current_depth)
      else
        output = output .. tostring(v)
      end

      has_children = true

      if next(t, _k) ~= nil then output = output .. ",\n" end
    end

    padding = pad(current_indent - 2)
    local maybe_newline = " "
    if has_children then maybe_newline = "\n" .. padding end
    return output .. maybe_newline .. "}"
  end

  return _tostring(object, indent)
end

-- options.min
-- options.max
-- options.default
function M.sanitize_numeric(value, options)
  if type(value) ~= "number" then return options.default end
  if options.min ~= nil and value < options.min then return options.default end
  if options.max ~= nil and value > options.max then return options.default end
  return value
end

---@class Geometry
---@field x integer
---@field y integer
---@field width integer
---@field height integer

---@class Screen
---@field geometry Geometry

---@class Tag
---@field clients Client[]

---@class Client
---@field floating boolean
---@field minimized boolean
---@field hidden boolean
---@field maximized boolean

---@alias edge
---| '"top_left"'
---| '"top"'
---| '"top_right"'
---| '"right"'
---| '"bottom_right"'
---| '"bottom"'
---| '"bottom_left"'
---| '"left"'

---Find the nearest edge relative to the supplied geometry.
---@param geo Geometry The geometry of the object
---@param s table The screen to find the edge of
---@return edge edge # The nearest edge
function M.find_nearest_edge(geo, s)
  local _, edge = awful.placement.closest_corner(
    { coords = function() return { x = geo.x, y = geo.y } end },
    { include_sides = true, parent = s }
  )
  return edge
end

---Given a screen, should it be classified as an ultrawide?
---@param s Screen
---@return boolean
function M.is_ultrawide(s)
  return s.geometry.width >= prefs.ultrawide_threshold
end

---Given a screen. is it in landscape mode?
---@param s Screen
---@return boolean
function M.is_landscape(s)
  return s.geometry.width > s.geometry.height
end

---Collect all clients of the supplied tag that are being tiled right now.
---The clients must also be visible, so not hidden, minimized, etc.
---@param tag Tag
---@return Client[]
function M.get_tiled_visible_clients(tag)
  local tiled_visible = {}
  for _, c in ipairs(tag:clients()) do
    if not c.floating and not c.minimized and not c.hidden and not c.maximized then
      tiled_visible[#tiled_visible + 1] = c
    end
  end
  return tiled_visible
end

return M
