local awful = require("awful")
local ruled = require("ruled")

local is_setup = false

local M
M = {
  init = function()
    if is_setup then return end
    is_setup = true

    for _, rule in pairs(M.client) do
      ruled.client.append_rule(rule)
    end
  end
}

M.client = {
  -- give focus to newly spawned clients and ensure they're visible
  {
    id = "global",
    rule = {},
    properties = {
      focus = awful.client.focus.filter,
      raise = true,
      screen = awful.screen.preferred,
      placement = awful.placement.centered + awful.placement.no_overlap + awful.placement.no_offscreen
    },
  },
  -- add titlebars to normal clients and dialogs
  {
    id = "titlebars",
    rule_any = { type = { "normal", "dialog" } },
    properties = { titlebars_enabled = true }
  },
  -- center dialogs regardless of overlap
  {
    id = "centered-dialogs",
    rule_any = { type = { "dialog" } },
    placement = awful.placement.centered
  }
}

M.init()
return M
