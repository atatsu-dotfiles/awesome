local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
local ruled = require("ruled")
local wibox = require("wibox")

local buttons = require("buttons")
local const = require("const")
local helpers = require("helpers")
local keys = require("keys")
local prefs = require("prefs")
local rules = require("rules")

local capi = {
  ---@diagnostic disable-next-line: undefined-global
  awesome = awesome,
  ---@diagnostic disable-next-line: undefined-global
  client = client,
  ---@diagnostic disable-next-line: undefined-global
  screen = screen,
  ---@diagnostic disable-next-line: undefined-global
  tag = tag,
}

--  * if not ultrawide
--    - master_fill_policy is always "expand"
--  * if ultrawide and only one client is _visible_
--    - master_fill_policy is "master_width_factor"
--  * if ultrawide and more than one client is _visible_
--    - master_fill_policy is "expand"
local set_master_fill_policy = function(t)
  if not helpers.is_ultrawide(t.screen) then
    t.master_fill_policy = const.EXPAND
    return
  end
  if #t:clients() == 1 then
    t.master_fill_policy = const.MASTER_WIDTH_FACTOR
    return
  end
  local visible_clients = {}
  for _, c in ipairs(t:clients()) do
    if not c.valid then goto continue end
    if c.type == const.DIALOG then goto continue end
    if c.floating then goto continue end
    if c.minimized then goto continue end
    if c.hidden then goto continue end
    visible_clients[#visible_clients + 1] = c
    ::continue::
  end
  if #visible_clients == 1 then
    t.master_fill_policy = const.MASTER_WIDTH_FACTOR
    return
  end
  t.master_fill_policy = const.EXPAND
end

local is_setup = false

local connect_signals = function(mapping, connect_signal)
  for signal, handler_or_handlers in pairs(mapping) do
    if type(handler_or_handlers) ~= "table" then
      connect_signal(signal, handler_or_handlers)
      goto continue
    end
    for _, handler in ipairs(handler_or_handlers) do
      connect_signal(signal, handler)
    end
    ::continue::
  end
end

local M
M = {
  init = function()
    if is_setup then return end
    is_setup = true

    connect_signals(M.awesome, capi.awesome.connect_signal)
    connect_signals(M.client, capi.client.connect_signal)
    connect_signals(M.tag, capi.tag.connect_signal)
    connect_signals(M.naughty, naughty.connect_signal)
    connect_signals(M.ruled.client, ruled.client.connect_signal)
    connect_signals(M.ruled.notification, ruled.notification.connect_signal)
    connect_signals(M.screen, capi.screen.connect_signal)
  end
}

M.awesome = {
  ["debug::error"] = (function()
    local in_error = false
    return function(err)
      -- prevent an endless error loop
      if in_error then return end
      in_error = true
      naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Error",
        text = tostring(err)
      })
      in_error = false
    end
  end)()
}

M.client = {
  focus = {
    -- When a client first gains focus adjust the border color so that it is highlighted
    -- and very visible. After a brief delay set the border color to
    -- beautiful.border_color_active.
    function(c)
      -- always check that it's valid in case it was closed during the execution
      -- of this callback
      if not c.valid then return end
      c.border_color = beautiful.border_color_focus
      -- unhighlight the client after a delay
      if not c.valid then return end
      c._timer = gears.timer.weak_start_new(prefs.focus_highlight_fade, function()
        -- client may have been closed before timer expired so check
        --if not c then return end
        if not c.valid then return end
        c.border_color = beautiful.border_color_active
        c._timer = nil
      end)
    end,
  },
  -- When a client loses focus set its border color to beautiful.border_color_normal.
  -- This exists so that the "focus" handler doesn't leave a bunch of highlighted clients
  -- if they are cycled through quick enough.
  unfocus = function(c)
    if c._timer ~= nil then c._timer:stop() end
    c.border_color = beautiful.border_color_normal
  end,
  ["request::manage"] = function(c)
    -- if the current layout isn't a floating one hide the titlebar
    -- but only of the client didn't spawn floating (dialogs)
    if awful.layout.get(c.screen) ~= awful.layout.suit.floating and not c.floating then
      awful.titlebar.hide(c)
    end

    -- New clients should be set to slave so that the master
    -- client remains static. I tried setting this up with a
    -- rule callback but it was being executed twice and consequently
    -- was messing stuff up.
    if #c.screen.clients < 2 then return end
    c:to_secondary_section()
  end,

  -- emitted when a client needs to get a titlebar
  ["request::titlebars"] = function(c)
    -- copying the stock config for now, will tweak later
    awful.titlebar(c).widget = {
      {
        -- left
        awful.titlebar.widget.iconwidget(c),
        buttons = buttons.titlebar(c),
        layout = wibox.layout.fixed.horizontal
      },
      {
        -- middle
        {
          -- title
          halign = "center",
          widget = awful.titlebar.widget.titlewidget(c)
        },
        buttons = buttons.titlebar(c),
        layout = wibox.layout.flex.horizontal
      },
      {
        -- right
        awful.titlebar.widget.floatingbutton(c),
        awful.titlebar.widget.maximizedbutton(c),
        awful.titlebar.widget.stickybutton(c),
        awful.titlebar.widget.ontopbutton(c),
        awful.titlebar.widget.closebutton(c),
        layout = wibox.layout.fixed.horizontal
      },
      layout = wibox.layout.align.horizontal
    }
  end,

  -- sloppy window focus
  ["mouse::enter"] = function(c)
    c:activate { context = "mouse_enter", raise = false }
  end,

  ["request::default_mousebindings"] = function()
    awful.mouse.append_client_mousebindings(buttons.client)
  end,

  ["request::default_keybindings"] = function()
    awful.keyboard.append_client_keybindings(keys.client)
  end,

  ["property::floating"] = function(c)
    for _, t in ipairs(c:tags()) do
      set_master_fill_policy(t)
    end
  end,

  ["property::minimized"] = function(c)
    for _, t in ipairs(c:tags()) do
      set_master_fill_policy(t)
    end
  end,

  ["property::hidden"] = function(c)
    for _, t in ipairs(c:tags()) do
      set_master_fill_policy(t)
    end
  end,
}

M.tag = {
  -- Whenever the layout changes check if it is floating or not.
  -- If it is now a floating layout, display client titlebars.
  -- If it is not floating, hide client titlebars.
  ["property::layout"] = function(t)
    local floating = t.layout == awful.layout.suit.floating
    for _, c in ipairs(t:clients()) do
      if floating then
        awful.titlebar.show(c)
      else
        awful.titlebar.hide(c)
      end
    end
  end,
  -- whenever clients get tagged/untagged check a few things and set the
  -- master_fill_policy in response:
  ["tagged"] = set_master_fill_policy,
  ["untagged"] = set_master_fill_policy,
}

M.naughty = {
  ["request::display_error"] = function(message, startup)
    naughty.notification {
      urgency = "critical",
      title = "Oops, an error occurred" .. (startup and " during startup!" or "!"),
      message = message
    }
  end,

  ["request::display"] = function(notification)
    naughty.layout.box {
      notification = notification,
    }
  end
}

M.ruled = {}

M.ruled.client = {
  ["request::rules"] = function()
    for _, rule in ipairs(rules.client) do
      ruled.client.append_rule(rule)
    end
  end
}

M.ruled.notification = {
  ["request::rules"] = function()
    ruled.notification.append_rule {
      rule = {},
      properties = {
        screen = awful.screen.preferred,
        implicit_timeout = 5,
        position = "top_left",
      }
    }
  end,
}

M.screen = {
  ["request::wallpaper"] = function(s)
    awful.wallpaper {
      screen = s,
      widget = {
        {
          image = beautiful.wallpaper,
          upscale = true,
          downscale = true,
          widget = wibox.widget.imagebox,
        },
        valign = "center",
        halign = "center",
        tiled = false,
        widget = wibox.container.tile,
      }
    }
  end
}

M.init()
return M
